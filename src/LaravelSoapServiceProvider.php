<?php

namespace Rudashi\LaravelSoap;

use Illuminate\Support\ServiceProvider;

class LaravelSoapServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() : void
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() : void
    {
        //
    }
}