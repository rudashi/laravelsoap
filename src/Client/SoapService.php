<?php

namespace Rudashi\LaravelSoap\Client;

class SoapService
{

    /**
     * @var null|string
     */
    protected $wsdl;

    /**
     * @var null|\SoapClient
     */
    protected $client;

    /**
     * @var bool
     */
    protected $certificate = false;

    /**
     * @var bool
     */
    protected $trace = false;

    /**
     * @var bool
     */
    protected $exceptions = false;

    /**
     * @var int
     */
    protected $soap_version = SOAP_1_2;

    /**
     * @var array
     */
    protected $options = [];

    /**
     * @var array
     */
    protected $classmap = [];

    /**
     * @var array
     */
    protected $headers = [];

    /**
     * @var int
     */
    protected $cache = WSDL_CACHE_NONE;

    /**
     * @var null|string
     */
    protected $location;

    public function __construct()
    {
        $this->wsdl         = null;
        $this->client       = null;
        $this->location     = null;
    }

    public function setWsdl(string $wsdl) : SoapService
    {
        $this->wsdl = $wsdl;

        return $this;
    }

    public function setClient(\SoapClient $client) : SoapService
    {
        $this->client = $client;

        return $this;
    }

    public function setCertificate(string $certificate) : SoapService
    {
        $this->certificate = $certificate;

        return $this;
    }

    public function setTrace(bool $trace) : SoapService
    {
        $this->trace = $trace;

        return $this;
    }

    public function setExceptions(bool $exceptions): SoapService
    {
        $this->exceptions = $exceptions;

        return $this;
    }

    public function setSoapVersion(int $soap_version) : SoapService
    {
        $this->soap_version = $soap_version;

        return $this;
    }

    public function setOptions(array $options) : SoapService
    {
        $this->options = $options;

        return $this;
    }

    public function extendsOptions(array $options)
    {
        $this->options = array_merge($options, $this->options);

        return $this;
    }

    public function setClassMap(array $classmap) : SoapService
    {
        $this->classmap = $classmap;

        return $this;
    }

    public function setCache(int $cache) : SoapService
    {
        $this->cache = $cache;

        return $this;
    }

    public function setHeader($namespace, $name, $data = null, $mustUnderstand = false) : SoapService
    {
        $this->headers[] = new \SoapHeader($namespace, $name, $data, $mustUnderstand);

        return $this;
    }

    public function setHeaders(array $headers = []) : SoapService
    {
        foreach ($headers as $header) {
            $this->setHeader($header['namespace'], $header['name'], $header['data'], $header['mustUnderstand']);
        }

        return $this;
    }

    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    public function getWsdl() : string
    {
        return $this->wsdl;
    }

    public function getClient() : ?\SoapClient
    {
        return $this->client;
    }

    public function getOptions() : array
    {
        $options = [
            'soap_version'  => $this->getSoapVersion(),
            'trace'         => $this->isTrace(),
            'exceptions'    => $this->isException(),
            'cache_wsdl'    => $this->getCache(),
            'classmap'      => $this->getClassmap(),
        ];
        if ($this->certificate !== false) {
            $options['local_cert'] = $this->certificate;
        }
        $this->options = array_merge($options, $this->options);

        return $this->options;
    }

    public function isTrace() : bool
    {
        return $this->trace;
    }

    public function getTrace() : bool
    {
        return $this->isTrace();
    }

    public function getCache() : int
    {
        return $this->cache;
    }

    public function getClassmap() : array
    {
        $classes  = [] ;

        foreach ($this->classmap as $originName => $class) {
            try {
                $classes[\is_int($originName) ? (new \ReflectionClass($class))->getShortName() : $originName] = $class;
            } catch (\ReflectionException $e) {}
        }

        return $classes;
    }

    public function isException() : bool
    {
        return $this->exceptions;
    }

    public function getException() : bool
    {
        return $this->isException();
    }

    public function getSoapVersion() : int
    {
        return $this->soap_version;
    }

    public function getHeaders() : array
    {
        return $this->headers;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

}