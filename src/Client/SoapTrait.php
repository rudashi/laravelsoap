<?php

namespace Rudashi\LaravelSoap\Client;

/**
 * @property Soap soap
 */
trait SoapTrait
{
    public function getLastRequestHeaders()
    {
        return $this->soap->getLastRequestHeaders(self::SERVICE);
    }

    public function getLastRequest()
    {
        return $this->soap->getLastRequest(self::SERVICE);
    }

    public function getLastResponseHeaders()
    {
        return $this->soap->getLastResponseHeaders(self::SERVICE);
    }

    public function getLastResponse()
    {
        return $this->soap->getLastResponse(self::SERVICE);
    }
}