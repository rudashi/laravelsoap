<?php

namespace Rudashi\LaravelSoap\Client\Contracts;

interface SoapClientInterface
{
    public function call($function_name, array $arguments, array $options = null, $input_headers = null, array &$output_headers = null);

    public function setHeaders(array $headers = []);

    public function setLocation($location = null);

    public function getLastRequestHeaders() : string;

    public function getLastRequest() : string;

    public function getLastResponseHeaders() : string;

    public function getLastResponse() : string;

}