<?php

namespace Rudashi\LaravelSoap\Client;

use Rudashi\LaravelSoap\Client\Contracts\SoapClientInterface;

class SoapClient extends \SoapClient implements SoapClientInterface
{

    public function __construct(string $wsdl, array $options = null, array $headers = [])
    {
        parent::__construct($wsdl, $options);

        $this->setHeaders($headers);
    }

    public function call($function_name, array $arguments, array $options = null, $input_headers = null, array &$output_headers = null)
    {
        return $this->__soapCall($function_name, $arguments, $options, $input_headers, $output_headers);
    }

    public function setHeaders(array $headers = []) : SoapClient
    {
        $this->__setSoapHeaders($headers);

        return $this;
    }

    public function setLocation($location = null) : SoapClient
    {
        $this->__setLocation($location);

        return $this;
    }

    public function getLastRequestHeaders() : string
    {
        return $this->__getLastRequestHeaders();
    }

    public function getLastRequest() : string
    {
        return $this->__getLastRequest();
    }

    public function getLastResponseHeaders() : string
    {
        return $this->__getLastResponseHeaders();
    }

    public function getLastResponse() : string
    {
        return $this->__getLastResponse();
    }

}