<?php

namespace Rudashi\LaravelSoap\Client;

use Rudashi\LaravelSoap\Exceptions\ServiceExistsException;
use Rudashi\LaravelSoap\Exceptions\ServiceNotExistsException;
use Rudashi\LaravelSoap\Exceptions\ServiceFunctionNotExistsException;

class Soap
{
    /**
     * @var array|SoapService[]
     */
    protected $services;

    public function __construct()
    {
        $this->services = [];
    }

    public function instance() : Soap
    {
        return $this;
    }

    public function addService(string $name, \Closure $closure) : Soap
    {
        if ($this->hasService($name) === false) {

            $service = new SoapService();
            $closure($service);
            $this->services[$name] = $service;

            return $this;
        }
        throw new ServiceExistsException("Service {$name} already exists.");
    }

    public function editService(string $name, \Closure $closure) : Soap
    {
        $closure($this->services[$name]);

        return $this;
    }

    /**
     * @param string $name
     * @param string|object|array $function
     * @param object|array $data
     * @param array $options
     * @param array $extraHeaders
     * @return mixed
     */
    public function call(string $name, $function, $data = [], array $options = [], array $extraHeaders = [])
    {
        if (strpos($name, '.')) {
            [$name, $function, $data, $options, $extraHeaders] = array_merge(explode('.', $name, 2), [$function, $data, $options]);
        }

        if ($function === null) {
            throw new ServiceFunctionNotExistsException("Service function for {$name} not exists.");
        }
        return $this->client($name, function (SoapClient $client) use ($function, $data, $options, $extraHeaders) {
            return $client->call($function, $data, $options, $extraHeaders);
        });
    }

    public function getLastRequestHeaders(string $name)
    {
        return $this->client($name, function (SoapClient $client) {
            return $client->getLastRequestHeaders();
        });
    }

    public function getLastRequest(string $name)
    {
        return $this->client($name, function (SoapClient $client) {
            return $client->getLastRequest();
        });
    }

    public function getLastResponseHeaders(string $name)
    {
        return $this->client($name, function (SoapClient $client) {
            return $client->getLastResponseHeaders();
        });
    }
    public function getLastResponse(string $name)
    {
        return $this->client($name, function (SoapClient $client) {
            return $client->getLastResponse();
        });
    }

    public function client(string $name, \Closure $closure)
    {
        if ($this->hasService($name)) {

            $service = $this->services[$name];

            if ($service->getClient() === null) {
                $client = new SoapClient($service->getWsdl(), $service->getOptions(), $service->getHeaders());
                $service->setClient($client);
            } else {
                $client = $service->getClient();
            }

            $client->setLocation($service->getLocation());

            return $closure($client);
        }
        throw new ServiceNotExistsException("Service {$name} not exists.");
    }

    public function extraHeader($namespace, $name, $data = null, $mustUnderstand = false) : \SoapHeader
    {
        return new \SoapHeader($namespace, $name, $data, $mustUnderstand);
    }

    public function extraHeaders(array $headers = []) : array
    {
        $extraHeaders = [];
        foreach ($headers as $header) {
            $extraHeaders[] = $this->extraHeader($header['namespace'], $header['name'], $header['data'], $header['mustUnderstand']);
        }
        return $extraHeaders;
    }

    public function hasService(string $name) : bool
    {
        return array_key_exists($name, $this->services);
    }

}