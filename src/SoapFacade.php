<?php

namespace Rudashi\LaravelSoap;

use Illuminate\Support\Facades\Facade;
use Rudashi\LaravelSoap\Client\Soap;

class SoapFacade extends Facade
{

    protected static function getFacadeAccessor() : string
    {
        return Soap::class;
    }
}