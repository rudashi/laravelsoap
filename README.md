Laravel SoapClient
================

[SoapClient](http://php.net/manual/en/class.soapclient.php) integration for Laravel.

General System Requirements
-------------
- [PHP >7.1.0](http://php.net/)
- [Laravel ~5.6.*](https://github.com/laravel/framework)

Quick Installation
-------------
If needed use composer to grab the library

```
$ composer require rudashi/laravel-soap
```

Usage
-------------

###Dependency Injection

```php
public function __construct(\Rudashi\LaravelSoap\Client\Soap $soap)
{
    $this->soap = $soap;
}
```

###Facade

```php
$soap = SoapFacade::instance();
```

###Add service

```php
$this->soap->add('CurrencyConvertor', function (SoapService $service) {
    $service->setWsdl(string)           // required, the WSDL url
        ->setCertificate(string)        // optional
        ->setTrace(bool)                // optional, default FALSE
        ->setExceptions(bool)           // optional, default FALSE
        ->setSoap_version(SOAP_1_2)     // optional, default SOAP_1_2
        ->setCache(WSDL_CACHE_NONE)     // optional, default WSDL_CACHE_NONE
        ->setHeader([...])              // optional
        ->setHeaders([])                // optional
        ->setOptions([])                // optional, extra Options
        ->setClassMap([]);              // optional, array of classes
});
```

###Call service

```php
$response = $soap->call('CurrencyConvertor.GetCurrencyRate', [
    'Currency'     => 'PLN',
    'RateDate'     => '2018-06-05',
]);
```

or with ClassMap (if not defined in service)

```php
$response = $soap->call(
    'CurrencyConvertor.GetCurrencyRate', 
    new GetCurrencyRateRequest('PLN', '2018-06-05')
);
```

It is possible to add additional data and headers with call

```php
$response = $soap->call(
    'CurrencyConvertor.GetCurrencyRate', 
    new GetCurrencyRateRequest('PLN', '2018-06-05'),
    $dataArray,
    $soap->extraHeaders([])
);
```

###Request Class
Example of request Class

File: GetCurrencyRateRequest.php
```php
<?php

namespace App\Http\Requests\Soap;

class GetCurrencyRateRequest
{
    protected $Currency;
    
    protected $RateDate;
    
    public function __construct($Currency, $RateDate)
    {
        $this->Currency = $Currency;
        $this->RateDate = $RateDate;
    }
}
```

###Response Class
Example of response Class. Class Name must be the same as response result.

File: GetCurrencyRateResponse.php
```php
<?php

namespace App\Http\Response\Soap;

class GetCurrencyRateResponse
{
    protected $GetCurrencyRateResult;
    
    public function __construct($GetCurrencyRateResult)
    {
        $this->GetCurrencyRateResult = $GetCurrencyRateResult;
    }
}
```

Authors
-------------

* **Borys Zmuda** - Lead designer - [GoldenLine](http://www.goldenline.pl/borys-zmuda/)